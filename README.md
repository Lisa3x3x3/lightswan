# DESCRIPTION  

This is a colour sensing ballerina.

Using a flora and colour sensor, the ballerina is able to detect colours and relay the value detected to a series of neopixels, controlled by a FLORA microcontroller. 
For this piece, we created a dress using fibre optics, which allowed for the entire dress to change to the colour output from the LEDs.

This was for a light performance piece in the 2015 Wellington LUX festival in New Zealand.

# README #


Run using the latest Arduino IDE (at time of creation, I was using v. 1.6.4). 


You will require the associated libraries for:

* Adafruit FLORA microcontroller

* Adafruit_TCS34725.h for the FLORA color sensor

* Adafruit_NeoPixel.h for driving neo pixels